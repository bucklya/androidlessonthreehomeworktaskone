package com.example.lessonthreehomeworktaskone;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText name;
    EditText day;
    EditText month;
    EditText year;

    Button action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        day = findViewById(R.id.day);
        month = findViewById(R.id.month);
        year = findViewById(R.id.year);
        action = findViewById(R.id.action);

        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!name.getText().toString().isEmpty() && !day.getText().toString().isEmpty() && !month.getText().toString().isEmpty() && !year.getText().toString().isEmpty()) {
                    String sName = name.getText().toString();
                    int sDay = Integer.parseInt(day.getText().toString());
                    int sMonth = Integer.parseInt(month.getText().toString());
                    int sYear = Integer.parseInt(year.getText().toString());

                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("name", sName);
                    intent.putExtra("day", sDay);
                    intent.putExtra("month", sMonth);
                    intent.putExtra("year", sYear);
                    startActivity(intent);
                } else{
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Ошибка")
                            .setMessage("Заполните все поля!!")
                            .setPositiveButton("Ок",new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog,int which){
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });
    }
}

