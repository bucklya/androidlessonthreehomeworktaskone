package com.example.lessonthreehomeworktaskone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SecondActivity extends AppCompatActivity {

    TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        info = findViewById(R.id.info);

        Bundle arguments = getIntent().getExtras();

        String name = arguments.get("name").toString();
        int day = arguments.getInt("day");
        int month = arguments.getInt("month");
        int year = arguments.getInt("year");

        Date currentDate = new Date();
        DateFormat dfDay = new SimpleDateFormat("dd", Locale.getDefault());
        String currentDay = dfDay.format(currentDate);
        int icd = Integer.parseInt(currentDay);
        DateFormat dfMonth = new SimpleDateFormat("MM", Locale.getDefault());
        String currentMonth = dfMonth.format(currentDate);
        int icm = Integer.parseInt(currentMonth);
        DateFormat dfYear = new SimpleDateFormat("yyyy", Locale.getDefault());
        String currentYear = dfYear.format(currentDate);
        int icy = Integer.parseInt(currentYear);

        if (day > icd) {
            month -= 1;
            day = 30 + icd - day;
        } else {
            day = icd - day;
        }

        if (month == 0) {
            year -= 1;
            month = 12;
        }
        if (month > icm) {
            year += 1;
            month = icm + 12 - month;
        } else {
            month = icm - month;
        }
        year = icy - year;

        info.setText(name + ", вы прожили: " + day + " день (дня/дней) " + month + " месяц (месяца/месяцев) " + year + " год (года/годов)");
    }
}
